package sheridan;

import java.util.regex.Pattern;

public class LoginValidator {

	public static boolean isValidLoginName( String loginName ) {
		Boolean returnCode = false;
	
    	if(Pattern.compile("^[a-zA-Z][A-Za-z0-9_]*$").matcher(loginName).find() || Pattern.compile("[A-Za-z0-9]{6,}").matcher(loginName).find()) {
					returnCode = true;
				}
		
		return returnCode;
	}
}
