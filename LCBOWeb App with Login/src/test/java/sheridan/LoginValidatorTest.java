package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class LoginValidatorTest {

	@Test
	public void testIsValidLoginRegular( ) {
		String loginName = "ldfajdlkf234";
		assertTrue("The test case passed", LoginValidator.isValidLoginName(loginName));
	}
	@Test
	public void testIsValidLoginException() {
		String loginName = "3434s";
		assertFalse("The test case failed", LoginValidator.isValidLoginName(loginName));
	}
	@Test
	public void testIsValidLoginBoundaryOut( ) {
		String loginName = "ldfajdlkf234";
		assertTrue("The test case passed", LoginValidator.isValidLoginName(loginName));
	}
	@Test
	public void testIsValidLoginBoundaryIN() {
		String loginName = "a3";
		assertTrue("The test case failed", LoginValidator.isValidLoginName(loginName));
	}
	@Test
	public void testisValidLoginBoundaryOut( ) {
		String loginName = "lsgf@";
		assertFalse("The test case passed", LoginValidator.isValidLoginName(loginName));
	}
	@Test
	public void testisValidLoginRegular() {
		String loginName = "34dfsadf34s";
		assertTrue("The test case failed", LoginValidator.isValidLoginName(loginName));
	}
	@Test
	public void testisValidLoginBoundaryIN() {
		String loginName = "ldfdd";
		assertTrue("The test case failed", LoginValidator.isValidLoginName(loginName));
		
	}
	@Test
	public void testisValidLoginException() {
		String loginName = "@dfs";
		assertFalse("The test case failed", LoginValidator.isValidLoginName(loginName));
	}

}
